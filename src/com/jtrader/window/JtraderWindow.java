package com.jtrader.window;

import java.io.IOException;
import java.time.LocalDateTime;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.jtrader.handlers.BtnCalculatorHandler;
import com.jtrader.handlers.BtnCurrentBalanceHandler;
import com.jtrader.handlers.BtnSaveHandler;
import com.jtrader.handlers.BtnShowFileHandler;
import com.jtrader.handlers.CheckBoxesHandler;
import com.jtrader.handlers.DifferenceLabelsHandler;
import com.jtrader.utils.ComboOptionsInitializer;
import com.jtrader.utils.Constants;
import com.jtrader.utils.Utils;

public class JtraderWindow {

	private Shell shlJtraderApplication;
	private static Display display;
	private Text tPrice;
	private Text tStopLose;
	private Text tTakeProfit;
	private Text tWinLose;
	private Label lDiffSL;
	private Label lDiffTP;
	private Button btnCalc;
	private Button cbTakeProfit;
	private Button cbStopLose;
	private Label lblTp;
	private Label lblSl;
	private Button rbBajista;
	private Button rbAlcista;
	private Label lblEntry;
	private DateTime dtPicker;
	private Combo cOptions;
	private Button btnCurrentBalance;
	private Button btnSave;
	private Button bShowFile;
	private Button cbLose;
	private Button cbWin;
	private Label lblBalance;
	private Label lblObservations;
	private Text tObservations;

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			JtraderWindow window = new JtraderWindow();
			display = Display.getDefault();
			Shell shell = window.open(display);

			while (!shell.isDisposed()) {
				if (!display.readAndDispatch()) {
					display.sleep();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public Shell open(Display display) {

		shlJtraderApplication = new Shell(display, SWT.SHELL_TRIM & (~SWT.RESIZE));
		shlJtraderApplication.setSize(534, 341);
		shlJtraderApplication.setText("Jtrader Application");
		createContents();
		shlJtraderApplication.open();
		shlJtraderApplication.layout();
		return shlJtraderApplication;
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {

		createBtnSave();

		createBtnCurrentBalance();

		createComboOptions();

		createDatePicker();

		createLabelEntry();

		createTextPrice();

		createRadioButtonAlcista();

		createRadioButtonBajista();

		createLabelStopLose();

		createLabelTakeProfit();

		createCheckBoxStopLose();

		createCheckBoxTakeProfit();

		createTextStopLose();

		createTextTakeProfit();

		createTextWinLose();

		createButtonCalculator();

		createLabelDiffSL();

		createLabelDiffTP();

		createButtonShowFile();

		createCbWin();

		createCbLose();

		createLabelBalance();

		createLabelObservations();

		createTextBoxObservations();

	}

	private void createTextBoxObservations() {
		tObservations = new Text(shlJtraderApplication, SWT.BORDER | SWT.MULTI | SWT.WRAP);
		tObservations.setBounds(319, 200, 176, 41);
		tObservations.setTextLimit(100);
	}

	private void createLabelObservations() {
		lblObservations = new Label(shlJtraderApplication, SWT.NONE);
		lblObservations.setBounds(320, 179, 91, 15);
		lblObservations.setText("Observaciones");
	}

	private void createLabelBalance() {
		lblBalance = new Label(shlJtraderApplication, SWT.NONE);
		lblBalance.setBounds(320, 127, 91, 15);
		lblBalance.setText("Balance Manual");
	}

	private void createCbLose() {
		cbLose = new Button(shlJtraderApplication, SWT.CHECK);
		cbLose.setBackground(new Color(Display.getCurrent(), 255, 0, 0));
		cbLose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CheckBoxesHandler handler = new CheckBoxesHandler();
				handler.handleLose(cbWin, cbLose, btnSave, tWinLose.getText());
			}
		});
		cbLose.setBounds(433, 165, 43, 16);
		cbLose.setText("-");

	}

	private void createCbWin() {
		cbWin = new Button(shlJtraderApplication, SWT.CHECK);
		cbWin.setBackground(new Color(Display.getCurrent(), 0, 255, 0));
		cbWin.setSelection(true);
		cbWin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CheckBoxesHandler handler = new CheckBoxesHandler();
				handler.handleWin(cbWin, cbLose, btnSave, tWinLose.getText());
			}
		});
		cbWin.setBounds(433, 143, 43, 16);
		cbWin.setText("+");
	}

	private void createButtonCalculator() {
		btnCalc = new Button(shlJtraderApplication, SWT.NONE);
		btnCalc.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BtnCalculatorHandler handler = new BtnCalculatorHandler();
				try {
					handler.handle();
				} catch (IOException ioEx) {
					MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("Error");
					messageBox.setMessage("Error abriendo la calculadora");
					messageBox.open();
					return;
				}
			}
		});
		btnCalc.setBounds(433, 45, 75, 58);
		btnCalc.setText("Calculadora");
	}

	private void createLabelDiffTP() {
		lDiffTP = new Label(shlJtraderApplication, SWT.NONE);
		lDiffTP.setBounds(188, 200, 55, 15);
	}

	private void createButtonShowFile() {
		bShowFile = new Button(shlJtraderApplication, SWT.NONE);
		bShowFile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BtnShowFileHandler showFileHandler = new BtnShowFileHandler();
				try {
					showFileHandler.handle();
				} catch (Exception ex) {
					MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("Error");
					messageBox.setMessage("Error abriendo el fichero de salida");
					messageBox.open();
					return;
				}
			}
		});
		bShowFile.setBounds(320, 278, 75, 25);
		bShowFile.setText("ShowFile");
		Utils.shouldEnableButton(bShowFile);
	}

	private void createLabelDiffSL() {
		lDiffSL = new Label(shlJtraderApplication, SWT.NONE);
		lDiffSL.setBounds(188, 144, 55, 15);

	}

	private void createTextWinLose() {
		tWinLose = new Text(shlJtraderApplication, SWT.BORDER);
		tWinLose.setBounds(320, 144, 107, 21);
		tWinLose.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {

				e.doit = Utils.onlyNumbersAdmitted(e.text);
			}
		});

		tWinLose.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {

				btnSave.setText(Constants.BTN_SAVE + " "
						+ (cbLose.getSelection() ? "-" + tWinLose.getText() : tWinLose.getText()));
				btnSave.setBackground(cbLose.getSelection() ? new Color(Display.getCurrent(), 255, 0, 0)
						: new Color(Display.getCurrent(), 0, 255, 0));

			}
		});

	}

	private void createTextTakeProfit() {
		tTakeProfit = new Text(shlJtraderApplication, SWT.BORDER);
		tTakeProfit.setBounds(71, 194, 99, 21);
		tTakeProfit.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				DifferenceLabelsHandler handler = new DifferenceLabelsHandler();
				handler.handle(lDiffTP, lDiffSL, rbAlcista, rbBajista, tPrice, tTakeProfit, tStopLose);

				// Si modificamos el valor en el texto y est� clickeado el check
				// de take profit , actualizamos el valor del
				// texto tWinLose y el literal del bot�n de guardar
				if (cbTakeProfit.getSelection()) {
					tWinLose.setText(lDiffTP.getText());
					btnSave.setText("Guardar " + lDiffTP.getText());
					if (lDiffTP.getText() != "" && tPrice.getText() != "") {
						btnSave.setBackground(
								lDiffTP.getText().contains("-") ? new Color(Display.getCurrent(), 255, 0, 0)
										: new Color(Display.getCurrent(), 0, 255, 0));
					}
				}
			}
		});
		tTakeProfit.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				e.doit = Utils.onlyNumbersAdmitted(e.text);
			}
		});
	}

	private void createTextStopLose() {
		tStopLose = new Text(shlJtraderApplication, SWT.BORDER);
		tStopLose.setBounds(71, 138, 99, 21);
		tStopLose.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				DifferenceLabelsHandler handler = new DifferenceLabelsHandler();
				handler.handle(lDiffTP, lDiffSL, rbAlcista, rbBajista, tPrice, tTakeProfit, tStopLose);
				// Si modificamos el valor en el texto y est� clickeado el check
				// de stop lose , actualizamos el valor del
				// texto tWinLose y el literal del bot�n de guardar
				if (cbStopLose.getSelection()) {
					tWinLose.setText(lDiffSL.getText());
					btnSave.setText("Guardar " + lDiffSL.getText());
					if (lDiffSL.getText() != "" && tPrice.getText() != "") {
						btnSave.setBackground(
								lDiffSL.getText().contains("-") ? new Color(Display.getCurrent(), 255, 0, 0)
										: new Color(Display.getCurrent(), 0, 255, 0));
					}
				}

			}
		});
		tStopLose.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				e.doit = Utils.onlyNumbersAdmitted(e.text);
			}
		});
	}

	private void createCheckBoxTakeProfit() {
		cbTakeProfit = new Button(shlJtraderApplication, SWT.CHECK);
		cbTakeProfit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CheckBoxesHandler handler = new CheckBoxesHandler();
				handler.handleTakeProfit(tPrice, cbStopLose, cbTakeProfit, tWinLose, cbWin, cbLose, lDiffTP, lblBalance,
						btnSave);

			}
		});
		cbTakeProfit.setBounds(42, 199, 13, 16);
	}

	private void createCheckBoxStopLose() {
		cbStopLose = new Button(shlJtraderApplication, SWT.CHECK);
		cbStopLose.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				CheckBoxesHandler handler = new CheckBoxesHandler();
				handler.handleStopLose(tPrice, cbStopLose, cbTakeProfit, tWinLose, cbWin, cbLose, lDiffSL, lblBalance,
						btnSave);
			}
		});
		cbStopLose.setBounds(42, 143, 13, 16);
	}

	private void createLabelTakeProfit() {
		lblTp = new Label(shlJtraderApplication, SWT.NONE);
		lblTp.setBounds(10, 200, 26, 15);
		lblTp.setText("T/P");
	}

	private void createLabelStopLose() {
		lblSl = new Label(shlJtraderApplication, SWT.NONE);
		lblSl.setBounds(10, 144, 26, 15);
		lblSl.setText("S/L");
	}

	private void createRadioButtonBajista() {
		rbBajista = new Button(shlJtraderApplication, SWT.RADIO);
		rbBajista.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DifferenceLabelsHandler handler = new DifferenceLabelsHandler();
				handler.handle(lDiffTP, lDiffSL, rbAlcista, rbBajista, tPrice, tTakeProfit, tStopLose);
			}
		});
		rbBajista.setBounds(226, 89, 90, 16);
		rbBajista.setText("Bajista");
	}

	private void createRadioButtonAlcista() {
		rbAlcista = new Button(shlJtraderApplication, SWT.RADIO);
		rbAlcista.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				DifferenceLabelsHandler handler = new DifferenceLabelsHandler();
				handler.handle(lDiffTP, lDiffSL, rbAlcista, rbBajista, tPrice, tTakeProfit, tStopLose);
			}
		});
		rbAlcista.setBounds(226, 66, 90, 16);
		rbAlcista.setText("Alcista");
		rbAlcista.setSelection(true);
	}

	private void createTextPrice() {
		tPrice = new Text(shlJtraderApplication, SWT.BORDER);
		tPrice.setBounds(71, 64, 99, 21);
		tPrice.setFocus();
		tPrice.addVerifyListener(new VerifyListener() {
			@Override
			public void verifyText(VerifyEvent e) {
				// S�lo dejamos si hemos introducido n�meros
				e.doit = Utils.onlyNumbersAdmitted(e.text);
			}
		});
		tPrice.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent arg0) {
				DifferenceLabelsHandler handler = new DifferenceLabelsHandler();
				handler.handle(lDiffTP, lDiffSL, rbAlcista, rbBajista, tPrice, tTakeProfit, tStopLose);
			}
		});
	}

	private void createLabelEntry() {
		lblEntry = new Label(shlJtraderApplication, SWT.NONE);
		lblEntry.setBounds(10, 67, 55, 15);
		lblEntry.setText("Entrada:");
	}

	private void createDatePicker() {
		dtPicker = new DateTime(shlJtraderApplication, SWT.BORDER | SWT.TIME);
		dtPicker.setBounds(401, 10, 107, 24);
	}

	private void createComboOptions() {
		cOptions = new Combo(shlJtraderApplication, SWT.DROP_DOWN);
		cOptions.setBounds(10, 10, 91, 23);
		ComboOptionsInitializer handler = new ComboOptionsInitializer();
		handler.initializeCombo(cOptions);
		cOptions.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				tPrice.setFocus();
				restartForm();
			}
		});
	}

	private void createBtnCurrentBalance() {
		btnCurrentBalance = new Button(shlJtraderApplication, SWT.NONE);
		btnCurrentBalance.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BtnCurrentBalanceHandler handler = new BtnCurrentBalanceHandler();
				try {
					handler.handle();
				} catch (IOException ioEx) {
					MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("Error");
					messageBox.setMessage("Error calculando el balance actual");
					messageBox.open();
					return;
				}

				MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_INFORMATION | SWT.OK);
				messageBox.setText("Guardado");
				messageBox.setMessage("Balance actual guardado correctamente");
				messageBox.open();
			}
		});
		btnCurrentBalance.setBounds(401, 278, 107, 25);
		btnCurrentBalance.setText("Balance Actual");
		Utils.shouldEnableButton(btnCurrentBalance);
	}

	private void createBtnSave() {
		btnSave = new Button(shlJtraderApplication, SWT.NONE);
		btnSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				BtnSaveHandler handler = new BtnSaveHandler();
				try {
					handler.handle(tStopLose.getText(), tTakeProfit.getText(), lDiffSL.getText(), lDiffTP.getText(),
							tWinLose.getText(),
							Utils.formatTimePickerValue(dtPicker.getHours(), dtPicker.getMinutes(),
									dtPicker.getSeconds()),
							cbStopLose.getSelection(), cbTakeProfit.getSelection(), cbLose.getSelection(),
							cOptions.getText(), tPrice.getText(), tObservations.getText());
				} catch (IOException ioex) {
					MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("Error");
					messageBox.setMessage("Error guardando datos en el fichero. Compruebe la Ruta de salida en el fichero de configuraci�n");
					messageBox.open();
					return;
				} catch (Exception ex) {
					MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_ERROR | SWT.OK);
					messageBox.setText("Error");	
					messageBox.setMessage("Error guardando datos en el fichero");
					messageBox.open();
					return;
			}

				MessageBox messageBox = new MessageBox(shlJtraderApplication, SWT.ICON_INFORMATION | SWT.OK);
				messageBox.setText("Guardado");
				messageBox.setMessage("Datos guardados correctamente");
				messageBox.open();

				restartForm();
			}
		});
		btnSave.setBackground(new Color(Display.getCurrent(), 0, 0, 0));
		btnSave.setBounds(320, 247, 188, 25);
		btnSave.setText(Constants.BTN_SAVE);
	}

	private void restartForm() {
		this.tPrice.setText("");
		this.tStopLose.setText("");
		this.lDiffSL.setText("");
		this.cbStopLose.setSelection(false);
		this.tTakeProfit.setText("");
		this.lDiffTP.setText("");
		this.cbTakeProfit.setSelection(false);
		this.rbAlcista.setSelection(true);
		this.rbBajista.setSelection(false);
		this.dtPicker.setTime(LocalDateTime.now().getHour(), LocalDateTime.now().getMinute(),
				LocalDateTime.now().getSecond());
		this.tWinLose.setText("");
		this.cbStopLose.setEnabled(true);
		this.cbStopLose.setEnabled(true);
		this.lblBalance.setVisible(true);
		this.tWinLose.setEnabled(true);
		this.tWinLose.setVisible(true);
		this.cbWin.setVisible(true);
		this.cbWin.setSelection(true);
		this.cbLose.setVisible(true);
		this.cbLose.setSelection(false);
		this.tPrice.setFocus();
		this.tObservations.setText("");
		this.btnSave.setBackground(new Color(Display.getCurrent(), 0, 0, 0));
		// Habilitamos el bot�n de ver el fichero, s�lo si el fichero existe
		Utils.shouldEnableButton(bShowFile);
		Utils.shouldEnableButton(btnCurrentBalance);
	}
}
