package com.jtrader.utils;

import org.eclipse.swt.widgets.Combo;

public class ComboOptionsInitializer {

	public void initializeCombo(Combo cOptions) {
		final String ibex = "Ibex35";
		final String nasdaq = "Nasdaq";
		final String dow = "DowJones";
		cOptions.add(ibex);
		cOptions.setData(ibex, "IBX35");
		cOptions.add(nasdaq);
		cOptions.setData(nasdaq, "NSDQ");
		cOptions.add(dow);
		cOptions.setData(dow, "DOW");
		cOptions.select(0);
	}

}
