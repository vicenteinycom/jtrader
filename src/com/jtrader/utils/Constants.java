package com.jtrader.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Properties;
import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

public final class Constants {

	public static final String EXIT_PATH;
	public static final String CALC_PATH;
	public static final String FILE_NAME = "ResumenActividad_" + dayOfMonthWith2Digits() + monthWith2Digits()
			+ LocalDateTime.now().getYear() + ".txt";
	public static final String BTN_SAVE = "Guardar";
	public static final String OPERATION_RESUME = "Resumen de la operaci�n";
	public static final String HOUR = "Hora";
	public static final String PRODUCT = "Producto";
	public static final String ENTRY_PRICE = "Precio de Entrada";
	public static final String DAY_INIT = "Inicio del d�a";
	public static final String SEPARATOR_13 = "************* ";
	public static final String SEPARATOR_3 = "*** ";
	public static final String BALANCE = "*** Balance = ";
	public static final String SEPARATOR_END_OPERATION_RESUME = "***************************************************";
	public static final String OBSERVATIONS = "Observaciones";
	public static final String STOP_LOSE = "StopLose";
	public static final String TAKE_PROFIT = "TakeProfit";
	public static final String SEPARATOR_CURRENT_BALANCE = StringUtils.LF
			+ "                               ********************" + StringUtils.LF;
	public static final String CURRENT_BALANCE = "                               Balance Actual = ";

	static {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("config.properties");
			prop.load(input);
			EXIT_PATH = prop.getProperty("RutaSalida");
			CALC_PATH = prop.getProperty("RutaCalculadora");
		} catch (IOException ex) {
			throw new RuntimeException("Cannot load config properties", ex);
		}finally{
			if (input!=null){
				
				try {
					input.close();
				} catch (IOException e) {
					throw new RuntimeException("Cannot load config properties", e);
				}
			}
	}

	}
	public static String dayOfMonthWith2Digits() {
		return StringUtils.right("0" + String.valueOf(LocalDateTime.now().getDayOfMonth()), 2);
	}

	public static String monthWith2Digits() {
		return StringUtils.right("0" + String.valueOf(LocalDateTime.now().getMonthValue()), 2);
	}
}
