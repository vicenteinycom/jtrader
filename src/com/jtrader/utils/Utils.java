package com.jtrader.utils;

import java.io.File;
import java.text.DecimalFormat;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.swt.widgets.Button;

public class Utils {

	public static String resta(String value1, String value2) {
		if (StringUtils.isNotEmpty(value1) && StringUtils.isNotEmpty(value2)) {
			Long v1 = Long.valueOf(value1);
			Long v2 = Long.valueOf(value2);

			return "" + (v1 - v2);
		} else {
			return "";
		}
	}

	public static String calculateDiffTP(boolean rbAlcistaChecked, String price, String tTakeProfit) {
		return (rbAlcistaChecked ? Utils.resta(tTakeProfit, price) : Utils.resta(price, tTakeProfit));
	}

	public static String calculateDiffSL(boolean rbBajistaChecked, String price, String tStopLose) {
		return (rbBajistaChecked ? Utils.resta(price, tStopLose) : Utils.resta(tStopLose, price));
	}

	public static boolean onlyNumbersAdmitted(String text) {
		boolean res = true;
		char[] chars = new char[text.length()];
		text.getChars(0, chars.length, chars, 0);
		for (int i = 0; i < chars.length; i++) {
			if (!('0' <= chars[i] && chars[i] <= '9')) {
				res = false;
			}
		}
		return res;
	}

	public static void shouldEnableButton(Button button) {
		try {
			File file = new File(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME);
			button.setEnabled(file.exists() ? true : false);
		} catch (Exception e) {
			// Si hay alg�n error deshabilitamos el bot�n
			button.setEnabled(false);
		}
	}

	public static String formatTimePickerValue(int hours, int minutes, int seconds) {
		DecimalFormat mFormat = new DecimalFormat("00");
		return mFormat.format(Double.valueOf(hours)) + ":" + mFormat.format(Double.valueOf(minutes)) + ":"
				+ mFormat.format(Double.valueOf(seconds));
	}
}
