package com.jtrader.unitTests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.junit.Test;

import com.jtrader.utils.Constants;
import com.jtrader.utils.Utils;

public class UtilsTest {

	@Test
	public void testRestaValue1LessThanValue2() {

		String value1 = "10200";
		String value2 = "10500";
		String res = Utils.resta(value1, value2);
		assertTrue("-300".equals(res));
	}

	@Test
	public void testRestaValue2LessThanValue1() {

		String value1 = "20000";
		String value2 = "15000";

		String res = Utils.resta(value1, value2);

		assertTrue("5000".equals(res));
	}

	@Test
	public void testCalculateDiffTPAlcista() {
		String entryPrice = "10000";
		String takeProfitValue = "12000";

		String res = Utils.calculateDiffTP(true, entryPrice, takeProfitValue);

		assertTrue("2000".equals(res));
	}

	@Test
	public void testCalculateDiffTPBajista() {
		String entryPrice = "10000";
		String takeProfitValue = "12000";

		String res = Utils.calculateDiffTP(false, entryPrice, takeProfitValue);

		assertTrue("-2000".equals(res));
	}

	@Test
	public void testCalculateDiffSLAlcista() {
		String entryPrice = "10000";
		String stopLoseValue = "9000";

		String res = Utils.calculateDiffSL(false, entryPrice, stopLoseValue);

		assertTrue("-1000".equals(res));
	}

	@Test
	public void testCalculateDiffSLBajista() {
		String entryPrice = "10000";
		String stopLoseValue = "11000";

		String res = Utils.calculateDiffSL(true, entryPrice, stopLoseValue);

		assertTrue("-1000".equals(res));
	}

	@Test
	public void testShouldEnableShowFileIfFileNotExists() {
		changeFieldValueByReflection();
		Display display = Display.getDefault();
		Button bShowFile = new Button(new Shell(display, SWT.CLOSE), SWT.NONE);
		bShowFile.setEnabled(true);

		Utils.shouldEnableButton(bShowFile);

		assertFalse(bShowFile.getEnabled());
	}

	@Test
	public void testOnlyNumbersAdmittedCorrectInput() {

		String test = "123";

		boolean res = Utils.onlyNumbersAdmitted(test);

		assertTrue(res);
	}

	@Test
	public void testOnlyNumbersAdmittedInCorrectInput() {

		String test = "123asfg";

		boolean res = Utils.onlyNumbersAdmitted(test);

		assertFalse(res);
	}

	@Test
	public void testShouldEnableShowFileIfFileExists() {
		Display display = Display.getDefault();
		Button bShowFile = new Button(new Shell(display, SWT.CLOSE), SWT.NONE);
		bShowFile.setEnabled(false);
		List<String> lines = Arrays.asList("First test Line", "The second test line");
		changeFieldValueByReflection();
		Path file = Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME);
		generateFakeTestFile(lines, file);

		Utils.shouldEnableButton(bShowFile);

		assertTrue(bShowFile.getEnabled());

		deleteFakeTestFile();
	}

	private void deleteFakeTestFile() {
		try {
			Files.delete(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME));
		} catch (IOException e) {
			fail(e.toString());
		}
	}

	private void generateFakeTestFile(List<String> lines, Path file) {
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			fail(e.toString());
		}
	}

	private void changeFieldValueByReflection() {
		try {
			// Le cambiamos el valor a la constante FILE_NAME por reflection
			setFinalStaticFileNameForTest(Constants.class.getDeclaredField("FILE_NAME"), "test.txt");
		} catch (NoSuchFieldException e1) {
			fail(e1.toString());
		} catch (SecurityException e1) {
			fail(e1.toString());
		} catch (Exception e1) {
			fail(e1.toString());
		}
	}

	private void setFinalStaticFileNameForTest(Field field, String newValue) throws Exception {
		field.setAccessible(true);
		Field modifiersField = Field.class.getDeclaredField("modifiers");
		modifiersField.setAccessible(true);
		modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
		field.set(null, newValue);
	}

}
