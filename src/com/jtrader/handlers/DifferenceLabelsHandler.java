package com.jtrader.handlers;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.jtrader.utils.Utils;

public class DifferenceLabelsHandler {

	public void handle(Label lDiffTP, Label lDiffSL, Button rbAlcista, Button rbBajista, Text tPrice, Text tTakeProfit,
			Text tStopLose) {

		// Actualizamos los labels de Diferencia de stop lose y take profit para
		// saber cuanto ganamos/perdemos
		lDiffTP.setText(Utils.calculateDiffTP(rbAlcista.getSelection(), tPrice.getText(), tTakeProfit.getText()));
		lDiffSL.setText(Utils.calculateDiffSL(rbBajista.getSelection(), tPrice.getText(), tStopLose.getText()));

	}

}
