package com.jtrader.handlers;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import com.jtrader.utils.Constants;

public class BtnShowFileHandler {

	public void handle() throws IOException {

		try {
			File myFile = new File(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME);
			Desktop.getDesktop().open(myFile);
		} catch (IOException e) {
			throw e;
		}
	}
}
