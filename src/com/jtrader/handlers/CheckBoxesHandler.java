package com.jtrader.handlers;

import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.jtrader.utils.Constants;

public class CheckBoxesHandler {

	public void handleStopLose(Text tPrice, Button cbStopLose, Button cbTakeProfit, Text tWinLose, Button cbWin,
			Button cbLose, Label lDiffSL, Label lblBalance, Button btnSave) {

		if (cbStopLose.getSelection()) {
			cbTakeProfit.setSelection(false);
			tWinLose.setEnabled(false);
			// Hacemos invisibles los cbs ,el input text y el label del balance
			// ya que el valor que vamos a guardar es el calculado en el label
			tWinLose.setVisible(false);
			cbWin.setVisible(false);
			cbLose.setVisible(false);
			lblBalance.setVisible(false);
			btnSave.setText(Constants.BTN_SAVE + " " + lDiffSL.getText());
			if (lDiffSL.getText() != "" && tPrice.getText() != "") {
				btnSave.setBackground(lDiffSL.getText().contains("-") ? new Color(Display.getCurrent(), 255, 0, 0)
						: new Color(Display.getCurrent(), 0, 255, 0));
			}

		} else {
			cbTakeProfit.setSelection(false);
			tWinLose.setEnabled(true);
			tWinLose.setVisible(true);
			tWinLose.setText("");
			cbWin.setEnabled(true);
			cbLose.setEnabled(true);
			cbWin.setVisible(true);
			cbLose.setVisible(true);
			lblBalance.setVisible(true);
			btnSave.setText(Constants.BTN_SAVE);
			btnSave.setBackground(new Color(Display.getCurrent(), 0, 0, 0));
		}

	}

	public void handleTakeProfit(Text tPrice, Button cbStopLose, Button cbTakeProfit, Text tWinLose, Button cbWin,
			Button cbLose, Label lDiffTP, Label lblBalance, Button btnSave) {

		if (cbTakeProfit.getSelection()) {
			cbStopLose.setSelection(false);
			tWinLose.setEnabled(false);
			// Hacemos invisibles los cbs ,el input text y el label del balance
			// ya que el valor que vamos a guardar es el calculado en el label
			tWinLose.setVisible(false);
			cbWin.setVisible(false);
			cbLose.setVisible(false);
			lblBalance.setVisible(false);
			btnSave.setText(Constants.BTN_SAVE + " " + lDiffTP.getText());
			if (lDiffTP.getText() != "" && tPrice.getText() != "") {
				btnSave.setBackground(lDiffTP.getText().contains("-") ? new Color(Display.getCurrent(), 255, 0, 0)
						: new Color(Display.getCurrent(), 0, 255, 0));
			}

		} else {
			cbStopLose.setSelection(false);
			tWinLose.setEnabled(true);
			tWinLose.setVisible(true);
			tWinLose.setText("");
			cbWin.setEnabled(true);
			cbLose.setEnabled(true);
			cbWin.setVisible(true);
			cbLose.setVisible(true);
			lblBalance.setVisible(true);
			btnSave.setText(Constants.BTN_SAVE);
			btnSave.setBackground(new Color(Display.getCurrent(), 0, 0, 0));
		}
	}

	public void handleWin(Button cbWin, Button cbLose, Button btnSave, String tWinLoseText) {

		// S�lo puede haber uno de los dos seleccionados
		if (cbWin.getSelection()) {
			cbLose.setSelection(false);
			btnSave.setBackground(new Color(Display.getCurrent(), 0, 255, 0));
			btnSave.setText(Constants.BTN_SAVE + " " + tWinLoseText);
		} else {
			cbLose.setSelection(true);
			btnSave.setBackground(new Color(Display.getCurrent(), 255, 0, 0));

		}

	}

	public void handleLose(Button cbWin, Button cbLose, Button btnSave, String tWinLoseText) {

		// S�lo puede haber uno de los dos seleccionados
		if (cbLose.getSelection()) {
			cbWin.setSelection(false);
			btnSave.setBackground(new Color(Display.getCurrent(), 255, 0, 0));
			btnSave.setText(Constants.BTN_SAVE + " " + "-" + tWinLoseText);
		} else {
			cbWin.setSelection(true);
			btnSave.setBackground(new Color(Display.getCurrent(), 0, 255, 0));
		}

	}

}
