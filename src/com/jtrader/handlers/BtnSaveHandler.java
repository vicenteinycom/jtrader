package com.jtrader.handlers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;

import org.apache.commons.lang3.StringUtils;

import com.jtrader.utils.Constants;

public class BtnSaveHandler {

	public void handle(String stopLoseText, String takeProfitText, String stopLoseValue, String takeProfitValue,
			String manualEntryValue, String hourMinuteSecond, boolean cbStopLoseChecked, boolean cbTakeProfitChecked,
			boolean cbLoseManualEntryChecked, String product, String entryPrice, String observations)
			throws IOException {

		StringBuilder sb = new StringBuilder();
		sb.append("");

		try {
			// Intentamos escribir en el fichero. Si ocurre una excepci�n del
			// tipo NoSuchFileException, lo creamos.
			// Cualquier otro error, lanzamos la excepci�n para mostrar un
			// mensaje de error
			appendTextInFile(stopLoseText, takeProfitText, stopLoseValue, takeProfitValue, manualEntryValue,
					cbStopLoseChecked, cbTakeProfitChecked, cbLoseManualEntryChecked, hourMinuteSecond, product,
					entryPrice, observations, sb);

		} catch (NoSuchFileException ex) {
			try {
				createNewFileAndAppend(stopLoseText, takeProfitText, stopLoseValue, takeProfitValue, manualEntryValue,
						cbStopLoseChecked, cbTakeProfitChecked, cbLoseManualEntryChecked, hourMinuteSecond, product,
						entryPrice, observations, sb);
			} catch (IOException ioex) {
				throw ioex;
			}
		} catch (IOException e) {
			throw e;
		}
	}

	private void createNewFileAndAppend(String stopLoseText, String takeProfitText, String stopLoseValue,
			String takeProfitValue, String manualEntryValue, boolean cbStopLoseChecked, boolean cbTakeProfitChecked,
			boolean cbLoseManualEntryChecked, String hourMinuteSecond, String product, String entryPrice,
			String observations, StringBuilder sb) throws IOException {

		generateNewOperationsDay(sb);
		generateNewOperationResume(sb, stopLoseText, takeProfitText, hourMinuteSecond, product, entryPrice,
				observations, stopLoseValue, takeProfitValue, manualEntryValue, cbStopLoseChecked, cbTakeProfitChecked,
				cbLoseManualEntryChecked);

		try {
			Files.write(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME), sb.toString().getBytes(),
					StandardOpenOption.CREATE);
		} catch (IOException ioex) {
			throw ioex;
		}

	}

	private void appendTextInFile(String stopLoseText, String takeProfitText, String stopLoseValue,
			String takeProfitValue, String manualEntryValue, boolean cbLoseChecked, boolean cbTakeProfitChecked,
			boolean cbLoseManualEntryChecked, String hourMinuteSecond, String product, String entryPrice,
			String observations, StringBuilder sb) throws IOException {
		try {
			Files.write(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME), StringUtils.LF.getBytes(),
					StandardOpenOption.APPEND);
			generateNewOperationResume(sb, stopLoseText, takeProfitText, hourMinuteSecond, product, entryPrice,
					observations, stopLoseValue, takeProfitValue, manualEntryValue, cbLoseChecked, cbTakeProfitChecked,
					cbLoseManualEntryChecked);
			Files.write(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME), sb.toString().getBytes(),
					StandardOpenOption.APPEND);

		} catch (NoSuchFileException e) {
			throw e;
		} catch (IOException ioex) {
			throw ioex;
		}

	}

	private void generateNewOperationResume(StringBuilder sb, String stopLoseText, String takeProfitText,
			String hourMinuteSecond, String product, String entryPrice, String observations, String stopLoseValue,
			String takeProfitValue, String manualEntryValue, boolean cbStopLoseChecked, boolean cbTakeProfitChecked,
			boolean cbLoseManualEntryChecked) {
		sb.append(Constants.SEPARATOR_13 + Constants.OPERATION_RESUME + " " + Constants.SEPARATOR_13 + StringUtils.LF);
		sb.append(Constants.SEPARATOR_13 + Constants.HOUR + ": " + hourMinuteSecond + StringUtils.LF);
		sb.append(Constants.SEPARATOR_13 + Constants.PRODUCT + ": " + product + StringUtils.LF);
		sb.append(Constants.SEPARATOR_3 + Constants.ENTRY_PRICE + ": " + entryPrice + StringUtils.LF);
		sb.append(Constants.SEPARATOR_3 + Constants.STOP_LOSE + " (" + stopLoseValue + ")" + ": " + stopLoseText
				+ (cbStopLoseChecked ? " <---- " : "") + StringUtils.LF);
		sb.append(Constants.SEPARATOR_3 + Constants.TAKE_PROFIT + " (" + takeProfitValue + ")" + ": " + takeProfitText
				+ (cbTakeProfitChecked ? " <---- " : "") + StringUtils.LF);
		sb.append(Constants.SEPARATOR_3 + Constants.OBSERVATIONS + ": " + observations + StringUtils.LF);
		// Para escribrir el balance:
		// -Si el check de S/L est� marcado, escribimos el valor calculado en el
		// label de stopLose
		// Sino, si el check de T/P est� marcado, escribimos el valor calculado
		// en el label de takeProfit
		// Sino, escribimos el valor que hemos introducido manualmente en el
		// input text de manualEntry (con un "-" si el check de cbLose est�
		// marcado)
		sb.append(Constants.SEPARATOR_3 + "Balance = "
				+ (cbStopLoseChecked ? stopLoseValue
						: cbTakeProfitChecked ? takeProfitValue
								: cbLoseManualEntryChecked ? "-" + manualEntryValue : manualEntryValue)
				+ StringUtils.LF);
		sb.append(Constants.SEPARATOR_END_OPERATION_RESUME);

	}

	private void generateNewOperationsDay(StringBuilder sb) {

		sb.append(Constants.SEPARATOR_13 + Constants.DAY_INIT + " " + Constants.dayOfMonthWith2Digits() + " "
				+ Constants.monthWith2Digits() + " " + LocalDateTime.now().getYear() + " " + Constants.SEPARATOR_13
				+ StringUtils.LF);
		sb.append(Constants.SEPARATOR_13 + StringUtils.LF);

	}

}
