package com.jtrader.handlers;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import com.jtrader.utils.Constants;

public class BtnCurrentBalanceHandler {

	public void handle() throws IOException {
		try {
			// Intentamos escribir en el fichero. S�lo podemos llegar aqu� si el
			// fichero est� creado (sino, el bot�n est� deshabilitado)
			appendCurrentBalance();

		} catch (NoSuchFileException ex) {
			throw ex;
		} catch (IOException e) {
			throw e;
		} catch (Exception excep) {
			throw excep;
		}

	}

	private void appendCurrentBalance() throws IOException {

		StringBuilder sb = new StringBuilder();
		sb.append("");
		try {
			List<String> lines = Files.readAllLines(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME),
					StandardCharsets.ISO_8859_1);
			List<Integer> allBalance = new ArrayList<Integer>();
			int currentBalance = 0;

			// Recorremos todas las l�neas buscando el Balance de cada operaci�n
			for (String line : lines) {
				if (line.contains(Constants.BALANCE)){
					String balanceFromLine = line.split("=")[1].trim();
					if (!"".equals(balanceFromLine)){
						allBalance.add(Integer.parseInt(balanceFromLine));
					}
				}
			}

			// Calculamos el balance actual
			for (Integer item : allBalance) {
				currentBalance += item;
			}

			sb.append(Constants.SEPARATOR_CURRENT_BALANCE);
			sb.append(Constants.CURRENT_BALANCE + currentBalance);
			sb.append(Constants.SEPARATOR_CURRENT_BALANCE);

			Files.write(Paths.get(Constants.EXIT_PATH + "\\" + Constants.FILE_NAME), sb.toString().getBytes(),
					StandardOpenOption.APPEND);
		} catch (IOException e) {
			throw e;
		} catch (Exception ex) {
			throw ex;
		}
	}
}
